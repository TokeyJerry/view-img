/**
 * Created by Tiejianwen on 2019/12/04
 */

const base = process.env.VUEPRESS_BASH || '/'
const dest = process.env.VUEPRESS_DAST || 'public'
console.log('vuepress base: ', base)
console.log('vuepress dest: ', dest)

module.exports = {
    base,
    title: '瓜子流程中心',
    description: '瓜子流程中心',
    contentLoading: true,
    dest,
    markdown: {
        lineNumbers: true
    },
    themeConfig: {
        displayAllHeaders: true,
        sidebarDepth: 6,
        sidebar: [
            ['/guide/', '环境准备'],
            ['/develop/', '开发']
        ],
        nav: [
            { text: '环境准备', link: '/guide/' },
            { text: '开发', link: '/develop/' },
            { text: '发布地址', link: 'https://workflow-engine-sys.guazi-corp.com/#/', target: '_blank' },
        ],
        lastUpdated: '最后更新时间', // string | boolean
        // 假定是 GitHub. 同时也可以是一个完整的 GitLab URL
        repo: 'http://git.guazi-corp.com/SharingPlatform/workflow-engine-sys',
        // 自定义仓库链接文字。默认从 `themeConfig.repo` 中自动推断为
        // "GitHub"/"GitLab"/"Bitbucket" 其中之一，或是 "Source"。
        repoLabel: '查看源码',
        // 以下为可选的编辑链接选项
        docsDir: 'docs',
        // 假如文档放在一个特定的分支下：
        docsBranch: 'master',
        // 默认是 false, 设置为 true 来启用
        editLinks: true,
        // 默认为 "Edit this page"
        editLinkText: '帮助我们改善此页面！'
    }
}
