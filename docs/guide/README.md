
# 环境准备

## 一、Clone Or DownLoad

```sh
# clone branch online
git clone -b online git@git.guazi-corp.com:SharingPlatform/workflow-engine-sys.git

# new develop branch
git checkout -b branch-develop

# add dependences 
yarn 
# or
npm install
```

## 二、Local Environment Configuration

### 1.nginx 安装及配置

> 用于[流程编辑器](http://git.guazi-corp.com/SharingPlatform/guagua-processOn)开发的静态文件重定向
>
> [Homebrew 工具安装](https://brew.sh/)

- **安装**

```sh
# 1.利用 Homebrew 安装，Mac 默认安装路径为：/usr/local/etc/nginx
sudo brew install nginx

# 2.查看版本
nginx -v

# 3.启动 Nginx
sudo nginx 

# 4.查看是否启动成功
ps aux |grep nginx

# 显示如下 表示启动成功
    nobody           53741   0.0  0.0  4299880    576   ??  S    11:22上午   0:00.09 nginx: worker process
    root              8417   0.0  0.0  4299644    584   ??  Ss   四11上午   0:00.04 nginx: master process nginx
    tiejianwen       60389   0.0  0.0  4287208    928 s002  S+    2:39下午   0:00.00 grep nginx

# or 访问 http://localhost 出现 Welcome to Nginx

# 5.关闭 Nginx
sudo nginx -s stop

# 6.修改配置文件后需要重新加载 Nginx
sudo nginx -s reload
```

- **配置**

```sh
server {
	listen       80;
        server_name  process-test.guazi-corp.com;  # 此处请勿修改，统一使用该域名
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host:$server_port; #这里是重点,这样配置才不会丢失端口
        proxy_set_header Cookie $http_cookie;
	proxy_buffering off;#配置可解决大文件请求不到的问题
	location /api/ {
	    proxy_pass http://127.0.0.1:7001/api/;
	}
	location /workflow {
	    proxy_pass http://127.0.0.1:8885/;
	}
	location /reception {
	    proxy_pass http://127.0.0.1:8886/;
	}
	location /workspace {
	    proxy_pass http://127.0.0.1:8881/;
	}
	location /admin {
	    proxy_pass http://127.0.0.1:8889/;
	}
	location /dist {
	    # 本地项目(流程编辑器)目录 git+http://git.guazi-corp.com/SharingPlatform/guagua-processOn
	    alias /Users/[自行替换用户目录]/Documents/works/process/guagua-processOn/dist/;
	}
}
```

当访问 `http://process-test.guazi-corp.com/dist` 时，出现如下图示表示配置成功

![流程编辑器](https://image.guazistatic.com/gz01181204/16/35/ad6bf842df2de906f32082370c939aac.png)

- **常见问题**

```sh
# 1.默认 80 端口被占用
nginx: [emerg] bind() to 0.0.0.0:80 failed (48: Address already in use)
# 修改 nginx.conf 文件里的端口号

# 2.权限不够
nginx: [alert] could not open error log file: open() “/usr/local/var/log/nginx/error.log” failed (13: Permission denied)
# nginx 需要 sudo 权限来启动
```

### 2.MongoDB 的安装、配置及启动

> 用于动态表单的新增、保存、发布等数据存储
>
> [Homebrew 工具安装](https://brew.sh/)

```sh
# 1.安装
brew install mongodb

# 2.环境变量配置，以方便全局目录启动
sudo vim ~/.bash_profile

# 将 MongoDB 的启动路径写入 将如下 [version] 替换为你安装的版本号
export PATH=$PATH:/usr/local/Cellar/mongodb/[version]/bin

# 3.创建本地数据库目录 db 【注意：根目录下创建目录需要 sudo 权限】
sodu mkdir /data/db

# 4.启动数据库
sudo mongod
```

## 三、Local Product Start

### 1.host 配置

```sh
# 开发环境
127.0.0.1 process-test.guazi-corp.com
10.16.208.36 workflow-engine-sys-server.guazi-corp.com # 动态表单文件上传服务地址
10.16.208.36 workflow.guazi.com # 动态表单发布后的地址

# 测试环境
10.16.208.205 wes-test.guazi-corp.com
10.16.208.36 workflow-engine-sys-server.guazi-corp.com # 动态表单文件上传服务地址
10.16.208.36 workflow.guazi.com # 动态表单发布后的地址
```

### 2.run dev

```sh
yarn start
# or
npm start
```

开发环境访问 [http://process-test.guazi-corp.com/workflow](http://process-test.guazi-corp.com/workflow)
