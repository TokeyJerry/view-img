---
home: true
actionText: 快速开发 →
actionLink: /guide/
features:
- title: 流程监管
  details: 用于审批流程的查询，并且对指定流程做移交和跳转等操作。
- title: 流程管理
  details: 用于展示现有的流程列表，并且可创建新的流程（新建、创建调试版本，编辑，保存，发布）。
- title: 动态表单
  details: 用于展示现有的动态表单，并且可创建新的表单项（新建、创建调试版本，编辑，保存，发布）。
- title: 流程数据
  details: 集成对已发布流程应用的统计、查询、导出表格等功能。
footer: MIT Licensed | Authored by tiejianwen@guazi.com
---

### 整体目录结构

> 各功能目录请详细参考功能页面

```
.
├── build ···················· 工程构建目录
│   └── webpack.config.js ···· webpack入口文件
├── med ······················ med配置文件
├── server ··················· 用于动态表单的 node 服务端
│   ├── controller
│   ├── middleware ··········· 中间件
│   ├── route
│   ├── services
│   └── stroage
├── src
│   ├── assets
│   ├── components
│   ├── libs
│   ├── network
│   ├── pages ················ 页面路径
│   ├── store
│   └── styles
├── target
├── app.js
├── global.config.js
└── process.json
```

### 项目结构分层

<div style="text-align: center;">
    <img src="https://img-blog.csdn.net/20180305105631792?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvcXFfMzU0NTE1NzI=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70">
    <p>项目结构分层图示</p>
</div>
